package com.jasy.employnepal;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.squareup.picasso.Picasso;

public class UserAdapter extends FirebaseRecyclerAdapter
        <User, UserAdapter.userViewHolder>{

//    db ko value lina option banako
    public UserAdapter(FirebaseRecyclerOptions<User> options){
        super(options);
    }

//    ViewHolder class
//    reference liyo list_layout.xml bata
    public static class userViewHolder extends RecyclerView.ViewHolder {
        TextView name_text, status_text;
        ImageView profile_image;

//        yo constuctor ho ...
        public userViewHolder(View itemView){
            super(itemView);

//            initialize the contents
            name_text = itemView.findViewById(R.id.name_text);
            status_text = itemView.findViewById(R.id.status_text);
            profile_image= itemView.findViewById(R.id.profile_image);
        }

    }

    @NonNull
    @Override
    public userViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        LayoutInflater le xml ko value lai View object ma lana sahayeta garxa
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_layout,parent,false);

        return new userViewHolder(view);
    }


    @Override
    protected void onBindViewHolder(@NonNull userViewHolder holder, int position, @NonNull  User model) {
//       onBindViewHolder le chai java ko data lai xml ma set garna
//        holder = ui vayo rey , model vanya data source horey {java ko data}
        holder.name_text.setText(model.getName());
        holder.status_text.setText(model.getStatus());
        Picasso.get().load(model.getImage()).into(holder.profile_image);


    }

}

