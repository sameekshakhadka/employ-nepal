package com.jasy.employnepal;


import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

public class MainActivity extends AppCompatActivity {


    private EditText mSearchField;
    private ImageButton mSearchBtn;
    private RecyclerView mResultList;

    private DatabaseReference mUserDatabase;

    UserAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        db instantiate garya
        mUserDatabase = FirebaseDatabase.getInstance().getReference();

        mSearchField = findViewById(R.id.search_field);
        mSearchBtn = findViewById(R.id.search_btn);

//        yo recuclerView ko ho reference ho -- result_list
        mResultList = findViewById(R.id.result_list);

        mResultList.setHasFixedSize(true);

//        to display recycler view linearly
        mResultList.setLayoutManager(new LinearLayoutManager(this));

//        firebase User search method - we need to perform search feature when user click the button
        mSearchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String searchText = mSearchField.getText().toString().trim();
                firebaseUserSearch(searchText);
            }
        });
    }

    private void firebaseUserSearch(String searchText) {

        Toast.makeText(this, "Start Searching ...", Toast.LENGTH_SHORT).show();

//        Search Query
        Query firebaseSearchQuery = mUserDatabase.child("Users").orderByChild("name").startAt(searchText).endAt(searchText+"\uf8ff");

        FirebaseRecyclerOptions<User> options = new FirebaseRecyclerOptions.Builder<User>().setQuery(firebaseSearchQuery, User.class)
       .build() ;

//          Create FirebaseRecyclerAdapter
//        FirebaseRecyclerAdapter<User,UserViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<User, UserViewHolder>(options)
//        {
//
//            @Override
//            public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//                View view = LayoutInflater.from(parent.getContext())
//                        .inflate(R.layout.list_layout, parent, false);
//                return new UserViewHolder(view);
//            }
//
//            @Override
//                        protected void onBindViewHolder(UserViewHolder holder, int position, User model) {
//
//            }
//
////            @Override
////            protected void populateViewHolder( UsersViewHolder viewHolder, User model, int position) {

////                viewHolder.setDetails(getApplicationContext(),model.getName(),model.getStatus(),model.getImage());
////            }
//        };
//        mResultList.setAdapter(firebaseRecyclerAdapter);

//        adapter.startListening();
        adapter = new UserAdapter(options);
        mResultList.setAdapter(adapter);
    }

    // view holder class
//
//    public static class UserViewHolder extends RecyclerView.ViewHolder{
//
//        View mView;
//
//        public UserViewHolder(View itemView) {
//            super(itemView);
//            mView = itemView;
//        }
//
//        public void setDetails(Context cntx,String userName, String userStatus, String userImage){
//            TextView user_status = (TextView) mView.findViewById(R.id.status_text);
//            TextView user_name = (TextView) mView.findViewById(R.id.name_text);
//            ImageView user_image = (ImageView) mView.findViewById(R.id.profile_image);
//
//            user_name.setText(userName);
//            user_status.setText(userStatus);

//            load(userImage) - url to load and into(user_image) - where to load
//            Glide.with(cntx).load(userImage).into(user_image);
//        }
//        }
    // Function to tell the app to start getting
    // data from database on starting of the activity
//    @Override protected void onStart()
//    {
//        super.onStart();
//        adapter.startListening();
//    }
//
//    // Function to tell the app to stop getting
//    // data from database on stopping of the activity
//    @Override protected void onStop()
//    {
//        super.onStop();
//        adapter.stopListening();
//    }

}

// Data fetch vairako xaina


